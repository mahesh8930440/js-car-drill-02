function problem1(inventory){
    
    function findTheDetails(carDetail){
        return carDetail.id===33
    }
    
    const aboutCar=inventory.find(findTheDetails);
    return `Car 33 is a ${aboutCar.car_year} ${aboutCar.car_make} ${aboutCar.car_model}`;
    
}
module.exports=problem1;
