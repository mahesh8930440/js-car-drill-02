function problem6(inventory){
    const allBMWAndAudiCars=inventory.filter(function(carDetails){
        if (carDetails.car_make==="BMW" || carDetails.car_make==="Audi"){
            return carDetails;
        }
    });
    return allBMWAndAudiCars;
}
module.exports=problem6;
