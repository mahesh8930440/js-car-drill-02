function problem3(inventory){
    const allCarModels=inventory
    .map(function(carDetails){
        return carDetails.car_model;
    })
    .sort(function(carModel1,carModel2){
        return ((carModel1.toLowerCase()).localeCompare(carModel2.toLowerCase()));
    });
    
    return allCarModels;    
    
}
module.exports=problem3;
    